# Angular Project Using REST methods.

Goal of this app was to:
- CREATE an employee
- GET all employees
- GET employee by id
- Edit employee with PUT method
- DELETE employee by id 

Frontend(Angular 5) App is hosted on Firebase. For backend I used .NET WebAPI 2.0 with SQL EXPRESS database. Both Database and .NET app is hosted on Cloud Azure.

Aditional features:
- Custom validation for confirmation of password
- Using guard(canActivate) to redirect to "notfound" page if id of the employee does not exist
- Using guard(canDeactivate) to ask for confirmation to discard changes on Create Employee page
- Using custom pipes to filter names in search box 
- Using resolver to open full view after data has been successfully loaded

To see live demo of this app:

https://employeesproject-bf3d5.firebaseapp.com/

More functionalities will be added later...

# AngularEmployee

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
