import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployeesParentComponent } from './employees-parent/employees-parent.component';
import { EmployeeCreateComponent } from './employees-parent/employee-create/employee-create.component';
import { CreateEmployeeCanDeactivateService } from './employees-parent/create-employee-can-deactivate-guard.service';
import { EmployeeDetailsComponent } from './employees-parent/employee-details/employee-details.component';
import { EmployeeListResorlverService } from './employees-parent/employee-list-resolver.service';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { EmployeeDetailsGuardService } from './shared/employee-details-guard.service';
import { EmployeeEditComponent } from './employee-edit/employee-edit.component';

const routes: Routes = [
  //resolve means after it finishes getting the data, only then load the view
  {path: 'list', component: EmployeesParentComponent, resolve: {employeeList: EmployeeListResorlverService} },
  {
    path: 'create',
    component: EmployeeCreateComponent,
    canDeactivate: [CreateEmployeeCanDeactivateService]
  },
  {path: 'edit/:id', component: EmployeeEditComponent},
  //more parameters: 'employees/:id/:gender/:age
  {path: 'employees/:id', component: EmployeeDetailsComponent, canActivate: [EmployeeDetailsGuardService] },
  {path: '', redirectTo: 'list', pathMatch: 'full'},
  {path: 'notfound', component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes 
    //{enableTracing: true}
  )],
  exports: [RouterModule]
})
export class AppRoutingModule { }
