import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Employee } from '../models/employee.model';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class EmployeesService {
  mainUrl = 'http://localhost:4099/api/employees/';
  departUrl = 'http://localhost:4099/api/departments';
  constructor(private http: Http) { }

  //add .delay(2000), for http to wait for 2 seconds
  getEmployees() {
    return this.http.get(this.mainUrl).map(res => res.json());
    //.delay(2000);
  }

  getEmployee(id: number): Observable<Employee> {
    return this.http.get(this.mainUrl + id).map(res => res.json());
  }

  getDepartments() {
    return this.http.get(this.departUrl).map(res => res.json());
  }

  CreateEmployee(employee) {
    return this.http.post(this.mainUrl, employee).map(res => res.json());
  }

  
  editEmployee(employee: Employee) {
    return this.http.put(this.mainUrl + employee.id, employee);
  }

  deleteEmployee(id: number){
    return this.http.delete(this.mainUrl + id);
  }
}
