import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import {BsDatepickerModule} from 'ngx-bootstrap/datepicker';
import { NgxSpinnerModule } from 'ngx-spinner';

import { AppComponent } from './app.component';
import {HttpModule} from '@angular/http';
import { EmployeesParentComponent } from './employees-parent/employees-parent.component';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/delay';
//for catching errors
import 'rxjs/add/observable/throw';
import { EmployeesService } from './shared/services/employees.service';
import { EmployeeCreateComponent } from './employees-parent/employee-create/employee-create.component';
import { SelectRequiredValidatorDirective } from './shared/select-required-validator.directive';
import { ConfirmEqualValidatorDirective } from './shared/confirm-equal-validator.directive';
import { DisplayEmployeeComponent } from './employees-parent/display-employee.component';
import { CreateEmployeeCanDeactivateService } from './employees-parent/create-employee-can-deactivate-guard.service';
import { EmployeeDetailsComponent } from './employees-parent/employee-details/employee-details.component';
import { EmployeeFilterPipe } from './employees-parent/employee-filter.pipe';
import { EmployeeListResorlverService } from './employees-parent/employee-list-resolver.service';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { EmployeeDetailsGuardService } from './shared/employee-details-guard.service';
import { EmployeeEditComponent } from './employee-edit/employee-edit.component';

@NgModule({
  declarations: [
    AppComponent,
    EmployeesParentComponent,
    EmployeeCreateComponent,
    SelectRequiredValidatorDirective,
    ConfirmEqualValidatorDirective,
    DisplayEmployeeComponent,
    EmployeeDetailsComponent,
    EmployeeFilterPipe,
    PageNotFoundComponent,
    EmployeeEditComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    FormsModule,
    BsDatepickerModule.forRoot(),
    
   
  ],
  providers: [EmployeesService, CreateEmployeeCanDeactivateService, EmployeeListResorlverService, EmployeeDetailsGuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
