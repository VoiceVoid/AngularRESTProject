import { EmployeeCreateComponent } from "./employee-create/employee-create.component";
import {CanDeactivate} from '@angular/router';
import { Injectable } from "@angular/core";

@Injectable()
export class CreateEmployeeCanDeactivateService implements CanDeactivate<EmployeeCreateComponent>{
    //If you wish to leave the page guard
    canDeactivate(component: EmployeeCreateComponent):  boolean {
        if(component.createEmployeeForm.dirty){
            //this asks for another true or false
            return confirm('Are you sure you want to discard your changes?');
        }
        return true;
    }   
}