import { Component, OnInit } from '@angular/core';
import { EmployeesService } from '../shared/services/employees.service';
import { Employee } from '../shared/models/employee.model';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-employees-parent',
  templateUrl: './employees-parent.component.html',
  styleUrls: ['./employees-parent.component.css']
})
export class EmployeesParentComponent implements OnInit {
  //TO TEST INPUTS AND SETTERS
  // employeeToDisplay: Employee = new Employee();
  // private arrayIndex = 1;


  employees: Employee[];
  filterEmployees: Employee[];

  //We change _searchTerm into a getter and setter property, so we can do some changes, when this property values change
  private _searchTerm: string;
  get searchTerm(): string {
    return this._searchTerm;
  }
  
  set searchTerm(value: string) {
    this._searchTerm = value;
    this.filterEmployees = this.filteredEmployees(value);
  }

  filteredEmployees(searchString: string) {
    return this.employees.filter(employee =>
      employee.name.toLowerCase().indexOf(searchString.toLowerCase()) !== -1);
  }


  constructor(private service: EmployeesService, private router: Router, private route: ActivatedRoute) { 
    this.employees = this.route.snapshot.data['employeeList'];
    if (this.route.snapshot.queryParamMap.has('searchTerm')) {
      this.searchTerm = this.route.snapshot.queryParamMap.get('searchTerm');
    } else {
      this.filterEmployees = this.employees;

    }
  }

  ngOnInit() {
    // this.service.getEmployees().subscribe(res => {

    //   this.employees = res;
    //   this.filterEmployees = res;
    //   console.log(this.employees);
    //   console.log('Subscribe :' + new Date().toTimeString());
    //   this.employeeToDisplay = this.employees[0];
    //   console.log(this.route.snapshot.queryParamMap.has('searchTerm'));
    //   console.log(this.route.snapshot.queryParamMap.getAll('searchTerm'));
    //   console.log(this.route.snapshot.queryParamMap.keys);
    //   console.log(this.route.snapshot.paramMap.keys);
    // });
    // if (this.route.snapshot.queryParamMap.has('searchTerm')) {
    //   this.searchTerm = this.route.snapshot.queryParamMap.get('searchTerm');
    // } else {
    //   this.filterEmployees = this.employees;

    // }

  }

  
  
  changeEmployeeName() {
    this.employees[0].name = 'Jordan';
    this.filterEmployees = this.filteredEmployees(this.searchTerm);
    // const newEmployeeArray: Employee[] = Object.assign([], this.employees); 
    // newEmployeeArray[0].name = 'Jordan';
    // this.employees = newEmployeeArray;
  }


  onClick(employeeID: number) {
    this.router.navigate(['/employees', employeeID], {
      queryParams: { 'searchTerm': this.searchTerm, 'testParam': 'testValue' }
    });
    //OR
    // in HTML [routerLink]

  }

  onDeleteNotification(id:number){
    this.service.deleteEmployee(id).subscribe();
    
    console.log('employee deleted');
  }
}
  //CHILD TO PARENT COMMUNICATION
  // dataFromChild: Employee;
  // handleNotify(eventData: any) {
  //   console.log('the picture was clicked in child component');
  //   this.dataFromChild = eventData;
  //   console.log(this.dataFromChild);
  // }


  //TO TEST SETTERS AND NGONCHANGES
  // nextEmployee(): void{
  //   if(this.arrayIndex <= 8){
  //     this.employeeToDisplay = this.employees[this.arrayIndex];
  //     this.arrayIndex++;
  //   } else{
  //     this.employeeToDisplay = this.employees[0];
  //     this.arrayIndex = 1;
  //   }
  // }

