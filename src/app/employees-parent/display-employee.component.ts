import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { Employee } from '../shared/models/employee.model';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployeesService } from '../shared/services/employees.service';

@Component({
  selector: 'app-display-employee',
  templateUrl: './display-employee.component.html',
  styleUrls: ['./display-employee.component.css']
})
// PROPERTY SETTER VS NGONCHANGES
export class DisplayEmployeeComponent implements OnInit {
  confirmDelete: false;
  private selectedEmployeeId: number;
  @Input() employee: Employee;
  @Input() searchTerm: string;
  @Output() notifyDelete: EventEmitter<number> = new EventEmitter<number>();
  constructor(private route: ActivatedRoute, private router: Router, private service: EmployeesService) { }

  ngOnInit() {
    this.selectedEmployeeId = +this.route.snapshot.paramMap.get('id');

    // console.log(this.employee);
  }

  getEmployeeNameAndGender(): string {
    return this.employee.name + ' ' + this.employee.gender;
  }


  viewEmployee(employeeID: number) {
    this.router.navigate(['/employees', this.employee.id], {
      queryParams: { 'searchTerm': this.searchTerm }
    });
  }

  editEmployee() {
    this.router.navigate(['/edit', this.employee.id]);
  }

  deleteEmployee(id: number) {
    
    this.notifyDelete.emit(id);
  }
}

    // CHILD TO PARENT COMMUNICATION
    //  @Output() notify: EventEmitter<any> = new EventEmitter<any>();

    // handleClick() {
    //   console.log("i clicked picture");
    //   this.notify.emit(this.employee);
    // }




    /////// TESTING FOR SETTERS:: GETTING ONLY ONE EMPLOYEE////////

    // private _employeeId: number;
    // @Input() 
    // set employeeId(val: number) {
    //   console.log('employeeId changed from ' + JSON.stringify(this._employeeId) + ' to ' + JSON.stringify(val));
    //   this._employeeId = val;
    // }

    // get employeeId(): number {
    //   return this._employeeId;
    // }


    // private _employee: Employee;
    // @Input()
    // set employee(val: Employee) {
    //   console.log('employee changed from ' + JSON.stringify(this._employee) + ' to ' + JSON.stringify(val));
    //   // console.log('Previous : ' + (this._employee ? this._employee.name : 'NULL'));
    //   // console.log('Current : ' + val.name);
    //   this._employee = val;
    // }

    // get employee(): Employee {
    //   return this._employee;
    // }

    // older Input
    // @input() employee: Employee;

    ///////////// TEST WITH ngOnChanges compared to setters/////////////
    // changes happen only on properties that change even though they are part of input
    // ngOnChanges(changes: SimpleChanges) {
    //   for(const propName of Object.keys(changes)){
    //     // console.log(propName);
    //     const change = changes[propName];
    //     const from = JSON.stringify(change.previousValue);
    //     const to = JSON.stringify(change.currentValue);

    //     console.log(propName + ' changed from ' + from + ' to ' + to);
    //   }
    // }


    // TEST FOR ngOnChanges, implement OnChanges
    // ngOnChanges(changes: SimpleChanges) {
    //   const previousEmployee = <Employee>changes.employee.previousValue;
    //   const currentEmployee = <Employee>changes.employee.currentValue;

    //   console.log('Previous : ' + (previousEmployee ? previousEmployee.name : 'NULL') );
    //   console.log('Current : ' + currentEmployee.name);
    // }


