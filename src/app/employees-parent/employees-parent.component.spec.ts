import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeesParentComponent } from './employees-parent.component';

describe('EmployeesParentComponent', () => {
  let component: EmployeesParentComponent;
  let fixture: ComponentFixture<EmployeesParentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeesParentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeesParentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
