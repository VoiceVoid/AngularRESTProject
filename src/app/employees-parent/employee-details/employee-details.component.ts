import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { EmployeesService } from '../../shared/services/employees.service';
import { Employee } from '../../shared/models/employee.model';

@Component({
  selector: 'app-employee-details',
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.css']
})
export class EmployeeDetailsComponent implements OnInit {
  private _id: number;
  employee: Employee = new Employee();
  constructor(private router: Router, private service: EmployeesService, private route: ActivatedRoute) { }

  ngOnInit() {
    // + means change it to number
    // this._id = +this.route.snapshot.paramMap.get('id');
    // this.service.getEmployee(this._id).subscribe(user => {
    // this.employee = user;
    // console.log(this.employee);
    // });
    this.getEmployee();
  }

  viewNextEmployee() {
    if(this.employee.id === null){
      this._id = this._id + 1;
    } else if(this.employee.id === 9) {
      this._id = 1;
    }
    this._id = this._id + 1;
    this.router.navigate(['/employees', this._id], {
      queryParamsHandling: 'preserve'
    });
    this.getEmployee();
  }

  getEmployee() {
    this.route.paramMap.subscribe(params => {
     this._id = +params.get('id');
     this.service.getEmployee(this._id).subscribe(user => {
      this.employee = user;
      console.log(this.employee);
    });
    });
    
  }

  
}
