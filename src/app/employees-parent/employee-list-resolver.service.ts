import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { Employee } from "../shared/models/employee.model";
import { Observable } from "rxjs/Observable";
import { EmployeesService } from "../shared/services/employees.service";
import { Injectable } from "@angular/core";

@Injectable()
export class EmployeeListResorlverService implements Resolve<Employee[]> {
    constructor(private service: EmployeesService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Employee[]> {
        return this.service.getEmployees();
    }
}