import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { EmployeesService } from '../../shared/services/employees.service';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { Employee } from '../../shared/models/employee.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-employee-create',
  templateUrl: './employee-create.component.html',
  styleUrls: ['./employee-create.component.css']
})
export class EmployeeCreateComponent implements OnInit {
  //we use partial since we only want to change one property on datepickerconfig
  datePickerConfig: Partial<BsDatepickerConfig>;

  @ViewChild('employeeForm') public createEmployeeForm: NgForm;

  previewPhoto = false;
  gender = 'male';
  isActive = true;
  department = 'IT';
  departments: any;
  employee: Employee = {
    id: null,
    name: null,
    gender: null,
    email: '',
    phoneNumber: null,
    contactPreference: null,
    dateOfBirth: null,
    department: 'select',
    isActive: null

  };
  //faster way of creating empty object
  emp: Employee = new Employee();
  constructor(private service: EmployeesService, private router: Router) {
    //we use object assign for copying property values from one or more source objects to a destination object
    //first parameter is destination object, 2nd parameter is source object
    this.datePickerConfig = Object.assign({},
      {
        containerClass: 'theme-dark-blue',
        // showWeekNumbers: false,
        // minDate: new Date(2018, 0,1),
        // maxDate: new Date(2018,11,310),
        dateInputForma: 'DD/MM/YYYY'
      });
  }

  ngOnInit() {
    this.service.getDepartments().subscribe(res => {
      this.departments = res;
      // console.log(this.departments);
    });


    // console.log(this.emp);
    // console.log(this.employee);
  }



  togglePhotoPreview() {
    this.previewPhoto = !this.previewPhoto;
  }

  //USE FORM VALUE(#employeeForm)
  // saveEmployeeWithForm(empForm: NgForm): void{
  //   console.log(empForm.value);
  // }

  //SAVE WITH OBJECT
  saveEmployee(): void {
    const newEmployee: Employee = Object.assign({}, this.employee);
    this.service.CreateEmployee(newEmployee).subscribe(user => {
      console.log(newEmployee);
      //console.log(this.employee);
    this.createEmployeeForm.reset();
      this.router.navigate(['list']);
    });

  }


}

