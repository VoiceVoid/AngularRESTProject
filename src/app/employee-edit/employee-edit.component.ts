import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { EmployeesService } from '../shared/services/employees.service';
import { Employee } from '../shared/models/employee.model';

@Component({
  selector: 'app-employee-edit',
  templateUrl: './employee-edit.component.html',
  styleUrls: ['./employee-edit.component.css']
})
export class EmployeeEditComponent implements OnInit {
  _id: number;
  employee: Employee = new Employee();
  constructor(private route: ActivatedRoute, private router: Router, private service: EmployeesService) { }

  ngOnInit() {
    this.getEmployee();
    console.log(this.employee);
  }

  getEmployee() {
    this.route.paramMap.subscribe(params => {
     this._id = +params.get('id');
     this.service.getEmployee(this._id).subscribe(user => {
      this.employee = user;
      console.log(this.employee);
    });
    });
    
  }
}
